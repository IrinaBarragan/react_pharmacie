import React from 'react';
import './input.scss';
import NavBar from './Components/NavBar';
import Pharma from './Components/Pharma';
import Garde from './Components/Garde';
import Add from './Components/Add';




class App extends React.Component {

  state = {
    showPage: 'pharma',
  }


  handleShowPage(str) {
    this.setState({ showPage: str })
  }

 render() {
    return (
      <div class="app">
        <NavBar page={(str) => this.handleShowPage(str)} />
        {this.state.showPage === 'pharma' && <Pharma />}
        {this.state.showPage === 'garde' && <Garde />}
        {this.state.showPage === 'add' && <Add page={(str) => this.handleShowPage(str)} />}
      
        
      </div>

    );
  }
}

export default App;
