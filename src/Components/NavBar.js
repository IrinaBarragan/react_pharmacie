import React from 'react';



class NavBar extends React.Component {

    clickPharm = (e) => {

        this.props.page('pharma')
    }
    clickGarde = (e) => {

        this.props.page('garde')
    }
    clickAdd = (e) => {

        this.props.page('add')
    }

    render() {
        return (
            <div id='navbar'>
                <button class="p" onClick={this.clickPharm} >Pharmacies</button>
                <button class="g" onClick={this.clickGarde}>Garde</button>
                <button class="a" onClick={this.clickAdd}>Ajoite une pharmacie</button>
            </div>
        )
    }
}

export default NavBar;