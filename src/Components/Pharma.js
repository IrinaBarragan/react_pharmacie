import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Card from './Card';
import UpdatePage from './UpdatePage'




const Pharma = (props) => {
    //les state, props
    const [data, setData] = useState([]);
    const [pharmaEdit, setPharmaEdit] = useState({});
    const [isLoaded, setisLoaded] = useState(false);
    const [upDate, setUpDate] = useState(false);

    useEffect(() => {
        // requete pour recupere les pharmacie
        axios.get('https://cherry-tart-07032.herokuapp.com/pharma')
            .then(response => {
                setisLoaded(true);
                //sort pour que l'affichage de plus recentes
                setData(response.data.sort((a, b) => b.id - a.id));
              
            })
            .catch((error) => {
                console.log(error);
            });
    }, [])

    // function de delete
    const deletePharma = (id) => {
        axios.delete(`https://cherry-tart-07032.herokuapp.com/pharma/${id}`)
            .then(response => {
                console.log('test', id);
                const newData = data.filter(parmacie => parmacie.id !== id);
                setData(newData);
            }
            )
    }

    //function d'affichage formulaire avec les datas de pharmacie choisié, lequelles je stocke dans un state PharmaEdit
    const updatePharma = (data) => {
        setPharmaEdit(data)
        setUpDate(true);
    }

    //function de souvgarde des modifications, leqielles on recevoie par les argumant du props pharma 
    const handleUpdate = (arg) => {
        axios.put(`https://cherry-tart-07032.herokuapp.com/pharma/${arg.id}`,
            {
                nom: arg.nom,
                quartier: arg.quartier,
                ville: arg.ville,
                garde: arg.garde
            })
            .then(response => {
                //reafichage les pharmacies avec les modifs
                const newData = data.map((item) => {
                    if (item.id === arg.id) {
                        return arg
                    }
                    return item
                })
                setData(newData)
                setUpDate(false)
            })
    }

    if (isLoaded) {
        if (upDate) {
            return (
                <UpdatePage pharma={(arg) => handleUpdate(arg)} upDate={upDate} pharmaEdit={pharmaEdit} />
            )
        }
        else {
            return (
                <ul>
                    { data.map(data => <Card key={data.id} delete={() => deletePharma(data.id)} update={() => updatePharma(data)} nom={data.nom} ville={data.ville} garde={data.garde} />)
                    }
                </ul>
            );
        }
    }
    else {
        return (<div>is loading</div>)
    }


}

export default Pharma;