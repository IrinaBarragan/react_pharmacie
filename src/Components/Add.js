import React from 'react';
import axios from 'axios';


class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'none',
      message: false,

    };

    this.handleChange = this.handleChange.bind(this);
    this.addPharma = this.addPharma.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  // affiche le list des pharmacie apres avoire appuer bouton Toutes les pharmacie
  handleChangePage = (e) => {
  
    this.props.page('pharma');
  }

  handleChange(event) {
    
    this.setState({ [event.target.id]: event.target.value });
  }
// suvgarde
  addPharma(event) {
    event.preventDefault();
    axios.post(`https://cherry-tart-07032.herokuapp.com/pharma`,
      {
        nom: this.state.nom,
        quartier: this.state.quartier,
        ville: this.state.ville,
        garde: this.state.garde
      })
      
    this.setState({ message: true })
  }

  render() {
    if (this.state.message) {
      return (
        <div>
          <form>
          <div>Nouvelle pharmacie a été ajoité</div>
          <button type="button" className='btn' onClick={this.handleChangePage}>Toutes les pharmacies</button>
          </form>
        </div>
      )
    } else {
      return (
        <div>
          <form >
            <div className="form-group">
              <label for="nom">Nom de la pharmacie</label>
              <input type="text" className="form-control" id="nom" value={this.state.nom} onChange={this.handleChange} placeholder="Entrez une pharmacie" required />
            </div>
            <div className="form-group">
              <label for="quartier">Quartier</label>
              <input type="text" className="form-control" id="quartier" value={this.state.quartier} onChange={this.handleChange} placeholder="Entrez un quartier" required/>
            </div>
            <div className="form-group">
              <label for="ville">Ville</label>
              <input type="text" className="form-control" id="ville" value={this.state.ville} onChange={this.handleChange} placeholder="Entrez une ville" required />
            </div>
            <select className="form-control" id="garde" value={this.state.garde} onChange={this.handleChange} required>
              <option>--Choisir jour de garde--</option>
              <option>lundi</option>
              <option>mardi</option>
              <option>mercredi</option>
              <option>jeudi</option>
              <option>vendredi</option>
              <option>samedi</option>
              <option>dimanche</option>
            </select>
            <button type="button" onClick={this.addPharma} className="btn">Submit</button>
          </form>
        </div>
      );
    }
  }
}

export default Add;