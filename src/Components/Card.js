import React from 'react';



class Card extends React.Component {
 
    render() {
        return (

            <div className="card">
                <div><p id="gardeCard">{this.props.garde}</p></div>
                <div className="cardPharm">
                    <div id="nomCard">{this.props.nom} </div>
                    <div><p id="villeCard">{this.props.ville}</p></div>
                </div>
                <div class='button'>
                    <button onClick={this.props.delete}>Supprimer</button>
                    <button onClick={this.props.update}>Modifier</button>
                </div>
            </div>
        )

    }
}




export default Card;

