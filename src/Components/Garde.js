import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CardGarde from './CardGarde';

const Garde = () => {
    const [data, setData] = useState([]);
    const [isLoaded, setisLoaded] = useState(false)
//requete pour afficher les pharmacie de garde
    useEffect(() => {
        axios.get('https://cherry-tart-07032.herokuapp.com/pharma-garde')
            .then(response => {
                setisLoaded(true);
                setData(response.data);
            })
            .catch((error) => {
                console.log(error);
            });
    }, [])
    if (isLoaded) {
        return (
            <ul>
                { data.map(data => {
                    return (
                        <CardGarde id={data.id} nom={data.nom} ville={data.ville} garde={data.garde} />)
                }
                )}
            </ul>

        )
    } else {
        return (<div>is loading</div>)
    }
}

export default Garde;