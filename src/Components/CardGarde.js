import React from 'react';


class CardGarde extends React.Component {
 
    render() {
        return (
            <div className="card">
                <div><p id="gardeCard">{this.props.garde}</p></div>
                <div className="cardPharm">
                    <div id="nomCard">{this.props.nom} </div>
                    <div><p id="villeCard">{this.props.ville}</p></div>
                </div>
            </div>
        )
    }
}
export default CardGarde;

