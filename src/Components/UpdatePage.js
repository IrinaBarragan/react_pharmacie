import React from 'react';


class UpdatePage extends React.Component {
  constructor(props) {
    super(props);
    console.log(props)
    this.state = {
      id: this.props.pharmaEdit.id,
      nom: this.props.pharmaEdit.nom,
      quartier: this.props.pharmaEdit.quartier,
      ville: this.props.pharmaEdit.ville,
      garde: this.props.pharmaEdit.garde,

    };
    this.handleChange = this.handleChange.bind(this);
    this.addPharma = this.addPharma.bind(this);
  }
  

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }
 
  addPharma=(event)=>{
    alert('Le nom a été soumis : ' + this.props.nom);
    event.preventDefault();

  }

  render() {


    return (
      <form >
        <div className="form-group">
          <label for="nom">Nom de la pharmacie</label>
          <input type="text" className="form-control" id="nom" value={this.state.nom} onChange={this.handleChange} />
        </div>
        <div className="form-group">
          <label for="quartier">Quartier</label>
          <input type="text" className="form-control" id="quartier" value={this.state.quartier} onChange={this.handleChange} />
        </div>
        <div className="form-group">
          <label for="ville">Ville</label>
          <input type="text" className="form-control" id="ville" value={this.state.ville} onChange={this.handleChange} />
        </div>
        <select className="form-control" id="garde" value='' onChange={this.handleChange} required>
          <option>--Choisir jour de garde--</option>
          <option>lundi</option>
          <option>mardi</option>
          <option>mercredi</option>
          <option>jeudi</option>
          <option>vendredi</option>
          <option>samedi</option>
          <option>dimanche</option>
        </select>
        <button type="button" onClick={()=>this.props.pharma(this.state)}  className="btn btn-primary">Submit</button>
      </form>
    );

  }
}



export default UpdatePage;